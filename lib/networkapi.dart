const BASE_URL = 'http://10.8.12.66/api/webusagi/flutter';

const GETALLS = BASE_URL + '/get-all-genre';
const ADDS = BASE_URL + '/insert-genre';
const UPDATES = BASE_URL + '/edit-genre';
const DELETES = BASE_URL + '/delete-genre';